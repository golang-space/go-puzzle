package main

import (
	"encoding/json"
	"fmt"
)

func parseNLP() {
	res := `{
"data": [
    {
        "synonym":"",
        "weight":"0.6",
        "word": "真丝",
        "tag":"材质"
    },
    {
        "synonym":"",
        "weight":"0.8",
        "word": "韩都衣舍",
        "tag":"品牌"
    },
    {
        "synonym":"连身裙;联衣裙",
        "weight":"1.0",
        "word": "连衣裙",
        "tag":"品类"
    }
]
}`

	// m := make(map[string]interface{})
	m := struct {
		Data []struct {
			Synonym string `json:"synonym"`
			Tag     string `json:"tag"`
		} `json:"data"`
	}{}

	err := json.Unmarshal([]byte(res), &m)
	if err != nil {
		panic(err)
	}

	// 用 map定义很简单，但是如果需要取某个值非常繁琐，建议自己定义结构
	// fmt.Printf("%+v\n", m["data"].([]interface{})[2].(map[string]interface{})["synonym"])

	fmt.Printf("%+v, %+v\n", m.Data[2].Synonym, m.Data[2].Tag)
}