package main

import (
	"encoding/json"
	"fmt"
)

type OrderItem struct {
	ID string	`json:"id"`
	Name string	`json:"name"`
	Price float64	`json:"price"`
}

type Order struct { // 给每个字段打标签,因为 go struct 里面的字段首字母需要大写
	ID string 	`json:"id"`
	Items []OrderItem `json:"items"` 
	Quantity int  `json:"quantity"`
	TotalPrice float64  `json:"tital_price"`
}

type Order2 struct { 
	ID string 	`json:"id"`
	Item OrderItem `json:"item"` // 这里可以使用指针 Item *OrderItem `json:"item"`
	Quantity int  `json:"quantity"`
	TotalPrice float64  `json:"tital_price"`
}

type Order1 struct { 
	ID string 	`json:"id"`
	Name string `json:"name"` // `json:"name, omitempty"` 省略空
	Quantity int  `json:"quantity"`
	TotalPrice float64  `json:"tital_price"`
}

// simple json format
func simpleJsonExample() {
	order1 := Order1 {
		ID: "1234",
		Name: "Learn go",
		Quantity: 3,
		TotalPrice: 30,
	}

	order2 := Order1 {
		ID: "1234",
		Quantity: 3,
		TotalPrice: 30,
	}

	json_value, err := json.Marshal(order1)
	json_value2, err := json.Marshal(order2)
	if err != nil {
		panic(err)
	}

	// 输出json 格式
	// {"ID":"1234","Name":"Learn go","Quantity":3,"TotalPrice":30}
	// 打完标签后，输出为小写
	// {"id":"1234","name":"Learn go","quantity":3,"tital_price":30}
	fmt.Printf("order1: %s\n",json_value)
	// order2:{"id":"1234","quantity":3,"tital_price":30}
	fmt.Printf("order2:%s\n",json_value2)

}

// 嵌套的json结构
func nestedJsonExample() {

	o1 := Order{
		ID: "1234",
		Quantity: 3,
		TotalPrice: 30,
		Items: []OrderItem{
			{
				ID: "item_1",
				Name: "item111",
				Price: 15,
			},
			{
				ID: "item_2",
				Name: "item222",
				Price: 15,
			},
		},
	}

	o2 := Order2{
		ID: "1234",
		Quantity: 3,
		TotalPrice: 30,
		Item: OrderItem{
			ID: "item_1",
			Name: "item111",
			Price: 15,
		},
	}

	json_value2, err := json.Marshal(o2)
	if err != nil {
		panic(err)
	}

	fmt.Printf("order1: %s\n",json_value2)

	json_value1, err := json.Marshal(o1)
	if err != nil {
		panic(err)
	}
	fmt.Printf("order1: %s\n",json_value1)
}

// unMarshal 反解析
func unMarshal() {
	s := `{"id":"1234","items":[{"id":"item_1","name":"learn go","price":15},{"id":"item_2","name":"interview","price":10}],"total_price":20}`

	var order1 Order
	err := json.Unmarshal([]byte(s), &order1)
	if err != nil {
		panic(err)
	}

	fmt.Printf("%+v\n", order1)
}

func main () {
	// simpleJsonExample()
	// nestedJsonExample()
	// unMarshal()
	parseNLP()
}