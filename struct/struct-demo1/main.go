package main

import "fmt"

// define struct
type AnimalCategory struct {
    kingdom string //
    phylum string
    class string
    order string
    family string
    genus string
    species string 
}

// 返回 string 类型的值
func (ac AnimalCategory) String() string {
   return fmt.Sprintf("%s%s%s%s%s%s%s",
		ac.kingdom, ac.phylum, ac.class, ac.order,
		ac.family, ac.genus, ac.species)
}

type Animal struct {
    scientificName string
    AnimalCategory // 动物基本分类
}

// 该方法会"屏蔽"掉嵌入字段中的同名方法。
func (a Animal) String() string {
    return fmt.Sprintf("%s (category: %s)",
    a.scientificName, a.AnimalCategory)
}

type Cat struct {
    name string
    Animal
}

// 该方法会"屏蔽"掉嵌入字段中的同名方法。
func (cat Cat) String() string {
	return fmt.Sprintf("%s (category: %s, name: %q)",
		cat.scientificName, cat.Animal.AnimalCategory, cat.name)
}

func main() {
    category := AnimalCategory{species: "cat", order:"order1"}
    fmt.Printf("The animal category: %s\n", category)

    animal := Animal{
        scientificName: "American Shorthair",
		AnimalCategory: category,
    }
    fmt.Printf("The animal: %s\n", animal)

    cat := Cat{
		name:   "little pig",
		Animal: animal,
	}
	fmt.Printf("The cat: %s\n", cat)
}
