package main

import (
	"fmt"
	"time"
)

func chanDemo1()  {
	ch1 := make(chan int, 3)
	ch1 <- 2
	ch1 <- -1
	ch1 <- 3
	// 从通道中接收一个元素值
	elem1 := <-ch1
	fmt.Printf("The first element received from channel ch1: %v\n",
		elem1)
}

// channel 作为参数,只能收数据？
func worker(id int, c chan int)  {
	// 不用显示调用 close()
	for n:= range c {
			fmt.Printf("Worker %d received %c\n",
								id, n)
		}
}

/**
chan <- int 表明这个 channel 只能用来送数据。收到这个channel 的人只能给它发数据。
不能从这个channel 获取数据
对应的是 <-chan int
*/
func createWorker(id int) chan<- int  {
	c := make(chan int)
	go worker(id, c)
	return c
}

func chanDemo()  {
	var channels [10]chan<- int
	for i := 0; i < 10; i++ {
		channels[i] = createWorker(i)
	}

	for i := 0; i < 10; i++ {
		channels[i] <- 'a' + i
	}

	for i := 0; i < 10; i++ {
		channels[i] <- 'A' + i
	}
	
	time.Sleep(time.Millisecond)
}

func bufferedChannel()  {
	c := make(chan int, 3)
	go worker(0 ,c)
	c <- 'a'
	c <- 'b'
	c <- 'c'
	c <- 'd'
	time.Sleep(time.Millisecond)
}

func channelClose()  {
	c := make(chan int)
	go worker(0, c)
	c <- 'a'
	c <- 'b'
	c <- 'c'
	c <- 'd'
	close(c)
	time.Sleep(time.Millisecond)
}

// error demo
func errorChannel()  {
	c := make(chan int)
	/*
		go func() {
		for {
			n := <-c
			fmt.Println(n)
		}
	}()
	*/
	c <- 1 // channel 用于两个 goroutine 之间通信，所以必须使用一个 goroutine 接收它
	c <- 2 // 获取创建 channel 时传一个int 参数
	n := <-c
	fmt.Println(n)
	time.Sleep(time.Millisecond)
	// 输出死锁
	// fatal error: all goroutines are asleep - deadlock!
}

func main() {
	chanDemo()
	fmt.Println("Buffered channel")
	bufferedChannel()
	fmt.Println("Channel close and range")
	channelClose()
	// errorChannel()
}
