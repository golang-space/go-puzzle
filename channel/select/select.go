package main

import (
	"fmt"
	"math/rand"
	"time"
)

// 最简单的 select
func simpleSelectDemo() {
	var c1, c2 chan int // c1 and c2 = nil
	select {
	case n := <-c1:
			fmt.Println("Received from c1:", n)
	case n := <-c2:
			fmt.Println("Received from c2:", n)
	default:
			fmt.Println("No value received")
	}
}	

/**
输出如下
Received from c1: 0
Received from c2: 0
Received from c2: 1
Received from c1: 1
Received from c2: 2
Received from c1: 2
Received from c1: 3
Received from c1: 4
Received from c2: 3
*/
func secondSelectDemo() {
	var c1, c2 = generator(),generator()

	for {
			select {
			case n := <-c1:
					fmt.Println("Received from c1:", n)
			case n := <-c2:
					fmt.Println("Received from c2:", n)
			}
	}
}	

// third
func thirdSelectDemo() {
	var c1, c2 = generator(),generator()
	var worker = createWorker(0)

	var values []int
	// 返回一个 <-chan time.Time. 10s 后会往这个 channel 输送一个时间
	tm := time.After(10 * time.Second) 
	// 定时，每隔一段时间输送一个值到chan
	tick := time.Tick(time.Second)

	for {
			var activeWorker chan<- int
			var activeValue int
			if (len(values) > 0) {
				activeWorker = worker
				activeValue = values[0]
			}

			select {
			case n := <-c1:
					values = append(values, n)
			case n := <-c2:
					values = append(values, n)
			case activeWorker <- activeValue:
					values = values[1:] // 	去除values 第一个元素
			
			case <-time.After(800 * time.Millisecond): // 每次 select 花的时间
				fmt.Println("timeout")
			case <-tick: 
				fmt.Println(
					"queue len =", len(values))
			case <-tm: // 10s 后收到数据
				fmt.Println("bye")
			return
		}
	}
}	

// 返回一个 int 类型chan
func generator() chan int {
	out := make(chan int)
	go func() {
		i := 0
		for {
			// 随机 sleep
			time.Sleep(
				time.Duration(rand.Intn(1500)) *
					time.Millisecond)
			out <- i
			i++
		}
	}()
	return out
}

func worker(id int, c chan int) {
	for n := range c {
		time.Sleep(time.Second)
		fmt.Printf("Worker %d received %d\n",
			id, n)
	}
}

func createWorker(id int) chan<- int {
	c := make(chan int)
	go worker(id, c)
	return c
}

func main() {
	// simpleSelectDemo()
	// secondSelectDemo()
	thirdSelectDemo()
}