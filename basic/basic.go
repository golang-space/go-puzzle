package main

import (
	"fmt"
	"math"
	"math/cmplx"
)

func variableZeroValue()  {
	var a int
	var s string
	fmt.Printf("%d, %q\n", a,s)
}

func variableInitialValue()  {
	var a,b int =  3,4
	var s string = "dev"
	fmt.Println(a, b, s)
}

func variableTypeDeduction()  {
	var a, b, c, s = 3, 4, true, "abc"
	fmt.Println(a, b, c, s)
}

// 简写
func variableShorter()  {
	a, b, c, s := 3, 4, true, "abc"
	fmt.Println(a, b, c, s)
}

func euler1()  {
	c := 3 + 4i
	fmt.Println(cmplx.Abs(c))
}

func euler2()  {
	fmt.Printf("%.3f\n",
		cmplx.Exp(1i*math.Pi) + 1)
}

func triangle()  {
	var a, b int = 3, 4
	fmt.Println(calcTriangle(a, b))
}

func calcTriangle(a, b int) int {
	var c int
	c = int(math.Sqrt(float64(a*a + b*b)))
	return c
}

// 常量(一般用大写)
// const filename = "abc.txt"
func consts()  {
	const (
		FILENAME = "abc.txt"
		a, b = 3 ,4
	)
	var c int
	c = int(math.Sqrt(float64(a*a + b*b)))
	fmt.Println(FILENAME, c)
}

// 枚举enums
func enums()  {
	const (
		cpp = iota
		_
		python
		golang
		javascript
	)

	const (
		b = 1 << (10 * iota)
		kb
		mb
		gb
		tb
		pb
	)

	fmt.Println(cpp, javascript, python, golang)
	fmt.Println(b, kb, mb, gb, tb, pb)
}

func main() {
	fmt.Println("Hello Go")
	variableZeroValue()
	variableInitialValue()
	variableTypeDeduction()
	variableShorter()
	euler1()
	euler2()
	triangle()
	consts()
	enums()
}