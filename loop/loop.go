package main

import (
  "io"
	"os"
	"fmt"
	"strconv"
	"bufio"
	"strings"
) 	

// 转换二进制
func convertToBinary(n int) string {
	result := ""

	for ; n > 0; n /= 2 {
		lsb := n % 2
		result = strconv.Itoa(lsb) + result
	} 

	return result
}

func printFile(filename string)  {
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}

	printFileContents(file)
}

func printFileContents(reader io.Reader)  {
	scanner := bufio.NewScanner(reader)

	// 省略条件，类似 while循环
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}

// 死循环
func forever()  {
	for {
		fmt.Println("abc")
	}
}

func main()  {

	fmt.Println(
		convertToBinary(5), // 101
		convertToBinary(13), // 1101
		convertToBinary(7238770),
		convertToBinary(0),
	)

	fmt.Println("abc.txt contents:")
	printFile("/Users/peterlxb/workspace/gitlab/go-puzzle/README.md")
	
	// forever()
	fmt.Println("printing a string:")

	s := `abc"d"
	kkkk
	123

	p`
	printFileContents(strings.NewReader(s))
}