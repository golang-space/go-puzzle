package main

import (
	"fmt"
	"runtime"
	"time"
)

func main()  {
 fmt.Println("Running in", runtime.Version())
	for i := 0;i < 1000; i++ {
		go func (i int)  {
			for {
				fmt.Printf("Hello from" +
					"goroutine %d\n", i)
			}
		}(i)
	}
	time.Sleep(time.Minute)
}

// 非抢占式多任务
// func main() {
// 	var a [10]int
// 	fmt.Println("Running in", runtime.Version())
// 	for i := 0; i < 10; i++ {
// 		go func(i int) {
// 			for {
// 				a[i]++
// 			}
// 		}(i)
// 	}
// 	time.Sleep(time.Millisecond)
// 	fmt.Println(a)
// }

/* 输出如下
Running in go1.16.6
[3241187 8186565 13355339 5449502 0 9767161 0 2766812 9717583 13995327]

里面的 goroutine 函数如果不传参数 i 直接运行会报错。
去掉参数 i int 后，a[i]++ 中的 i 引用的是 for 循环中的i, 当 main 函数执行完
i 会变成10，a[i] 引用这个值就会出现越界现象。
使用 go run -race goroutine.go 来检测这个报错信息。
*/
