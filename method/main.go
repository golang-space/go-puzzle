package main

import "fmt"

type Age int

func (age Age)LargeThan(a Age) bool {
	return age > a
} 

func (age *Age) Increase() {
	*age++ // 如果 age 是一个空指针，则此行将产生一个 panic
}

// 为自定义的函数类型 FilterFunc 声明方法
type FilterFunc func(in int) bool

func (ff FilterFunc) Filter(in int) bool {
	return ff(in)
}

// 为自定义的映射类型声明方法
type StringSet map[string]struct{}

func (ss StringSet) Has(key string) bool {
	_, present := ss[key]
	return present 
}

func (ss StringSet) Add(key string) {
	ss[key] = struct{}{}
}

func (ss StringSet) Remove(key string) {
	delete(ss, key)
}

// 为自定义的结构体类型 Book 和它的指针类型 *Book 声明fangf
type Book struct {
	pages int
}

type Books []Book

func (books Books) Modify() {
	// 对属主参数的间接部分的修改将反映到方法之外
	books[0].pages = 500
	// 对属主参数的直接部分的修改将不会反映到方法之外
	books = append(books, Book{789})
}

func (books Books) Modify2() {
	books = append(books, Book{789})
	books[0].pages = 500
}

func test_modify() {
	var books = Books{{123}, {456}}
	books.Modify()
	
	fmt.Println(books) // [{500}, {456}]
}

func test_modify2() {
	var books = Books{{123}, {456}}
	books.Modify2()
	
	fmt.Println(books) // [{123}, {456}]
}

func (b Book) Pages() int {
	return b.pages // 此函数体和 Book 类型的 Pages 方法体一样
}

func (b *Book) Pages2() int {
	return (*b).Pages()
}

func (b *Book) SetPages(pages int) {
	b.pages = pages // 此函数体和 *Book 类型的 SetPages 方法体一样
}

// test struct
func struct_test1() {
	var book Book
	// 调用这两个隐式声明的函数
	(*Book).SetPages(&book, 123)
	fmt.Println("Book.Pages: ", Book.Pages(book)) // 123 
}

func struct_test2() {
	var book Book

	fmt.Printf("%T \n", book.Pages) // func() int
	fmt.Printf("%T \n", (&book).SetPages) // func(int)
	// &book 值有一个隐式方法 Pages
	fmt.Printf("%T \n", (&book).Pages) // func() int

	// 调用三个方法
	(&book).SetPages(123)
	book.SetPages(123) // 等价于上一行
	fmt.Println(book.Pages()) // 123 
	fmt.Println((&book).Pages()) // 123 
}

// 方法值的估值
func test_value() {
	var b = Book{pages: 123}
	var p = &b
	var f1 = b.Pages
	var f2 = p.Pages
	var g1 = p.Pages2
	var g2 = b.Pages2

	b.pages = 789
	fmt.Println("f1(): ", f1()) // 123
	fmt.Println("f2(): ", f2()) // 123
	fmt.Println("g1(): ", g1()) // 789
	fmt.Println("g2(): ", g2()) // 789
}

func main() {
	struct_test1()
	struct_test2()

	//测试方法值和参数
	test_modify()
	test_modify2()
	test_value()
}