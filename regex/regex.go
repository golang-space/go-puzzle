package main

import (
	"fmt"
	"regexp"
)

const text = `
my email is ccmouse@gmail.com@abc.com
email1 is abc@def.org
email2 is    kkk@qq.com
email3 is ddd@abc.com.cn
`

func simpleReg() {
	re := regexp.MustCompile(
		`[a-zA-Z0-9]+@[a-zA-Z0-9.]+\.[a-zA-Z0-9]+`)
	match := re.FindAllString(text, -1)
	for _, m := range match {
		fmt.Println(m)
	}

	/*
	ccmouse@gmail.com
	abc@def.org
	kkk@qq.com
	ddd@abc.com.cn
	*/
}

// 匹配子串
func complexReg() {
	re := regexp.MustCompile(
		`([a-zA-Z0-9]+)@([a-zA-Z0-9]+)(\.[a-zA-Z0-9.]+)`)
	
	// 匹配所有子串，返回一个二维数组
	match := re.FindAllStringSubmatch(text, -1)
	for _, m := range match {
		fmt.Println(m)
	}
	/*
	[ccmouse@gmail.com ccmouse gmail .com]
	[abc@def.org abc def .org]
	[kkk@qq.com kkk qq .com]
	[ddd@abc.com.cn ddd abc .com.cn]
	*/
}

func main() {
	simpleReg()
	complexReg()
}