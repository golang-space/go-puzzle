package main

import "fmt"

// simple pointer example
func simple_p() {
  p0 := new(int) // p0 points to a zero int value ,初始化一个零值
  fmt.Println(p0) // 0xc000016090: 一个16进制地址描述符 
  fmt.Println(*p0) // 0

  // x is a copy of the value at the address stored in p0
  // x 是 p0 引用值的一个复制
  x := *p0
  // p1 p2 都存储 x 的地址，所以它们的值是一样的
  p1, p2 := &x, &x
  fmt.Println("p1 == p2: ",p1 == p2) // true
  fmt.Println("p0 == p1: ",p0 == p1) // false

  p3 := &*p0 // 等价 p3 := &(*p0) <=> p3 := p0
  // p3 和 p0 存储着一样的地址
  fmt.Println("p0 == p3: ",p0 == p3) // true

  *p0, *p1 = 123, 789
  fmt.Println("*p2, x, *p3: ", *p2, x, *p3) // 789 789 123

  fmt.Printf("%T, %T \n", *p0, x) // int int
  fmt.Printf("%T, %T \n", p0, p1) // *int *int
}

// 不会改变传入参数的值
func double(x int) {
  x += x
}

// 将函数的参数变为指针类型，
func double_with_pointer(x *int) {
  *x += *x
  x = nil // 
}

func why_p() {
  var a = 3
  var b = 3
  double(a)
  double_with_pointer(&b)
  fmt.Println("a: ",a) // 3:a 的值还是 3,传入 double() 函数里面的参数 a 只是变量 a 值的一个拷贝
  fmt.Println("b: ",b) // 6:
}

// go 指针不允许算数运算
func arithmetic_operation() {
  a := int64(5)
  p := &a

  /*
  * 下面的语句无法编译
  p++
  p = (&a) + 8
  */
  *p++
  fmt.Println("*p ,a: ", *p, a) // 6 6
  fmt.Println("p == &a: ", p == &a) // true

  *&a++
	*&*&a++
	**&p++
	*&*p++
  fmt.Println(*p, a) // 10 10
}

// 一个指针值不能被转换为一个任意的指针类型
// 一个指针值不能与任意指针类型的值进行比较
func complex_pointer_demo() {
  type MyInt int64
  type Ta    *int64
  type Tb    *MyInt

  // 4 nil pointers of different types.
  var pa0 Ta
  var pa1 *int64
  var pb0 Tb
  var pb1 *MyInt

  // all return true
  fmt.Println("pa0 == pa1: ",pa0 == pa1)
  fmt.Println("pb0 == pb1: ",pb0 == pb1)

  // invalid operation: pa0 == pb0 (mismatched types Ta and Tb)
  // fmt.Println("pa0 == pb0: ",pa0 == pb0) // pa1 == pb1
  
}

func main() {
  simple_p()
  fmt.Println("second example")
  why_p()
  fmt.Println("arithmetic_operation")
  arithmetic_operation()
  fmt.Println("complex_pointer_demo")
  complex_pointer_demo()
}
